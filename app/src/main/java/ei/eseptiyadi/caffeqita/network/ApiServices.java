package ei.eseptiyadi.caffeqita.network;

import ei.eseptiyadi.caffeqita.model.ResponseDetailTransaksi;
import ei.eseptiyadi.caffeqita.model.ResponseListTransaksi;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiServices {

    @GET("listtransaksi.php")
    Call<ResponseListTransaksi> listtransaksi();

    @FormUrlEncoded
    @POST("aksestransaksi.php")
    Call<ResponseDetailTransaksi> detailtransaksi(
            @Field("kode_transaksi") String kodetransaksi
    );

}
