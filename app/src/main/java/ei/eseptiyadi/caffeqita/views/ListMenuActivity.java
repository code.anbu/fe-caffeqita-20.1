package ei.eseptiyadi.caffeqita.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import ei.eseptiyadi.caffeqita.R;

public class ListMenuActivity extends AppCompatActivity {

    FloatingActionButton fabAddMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_menu);

        fabAddMenuItem = (FloatingActionButton)findViewById(R.id.fabAddMenu);

        fabAddMenuItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ListMenuActivity.this, "Ini floating action click", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void RefreshListMenu() {

    }
}